package jwt;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.*;

import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@RunWith(MockitoJUnitRunner.class)
public class JwtBuilderTest {

    @Spy
    private JwtBuilder builder;

    /**
     * Obtain token with client_secret_jwt authentication
     * @result Access Token should be obtained without any errors
     */
    @Test
    public void obtainTokenTest() throws Exception{
        CloseableHttpResponse httpResponse = Mockito.mock(CloseableHttpResponse.class);
        HttpEntity httpEntity = Mockito.mock(HttpEntity.class);
        CloseableHttpClient httpClient = Mockito.mock(CloseableHttpClient.class);
        Mockito.when(builder.getHttpClient()).thenReturn(httpClient);
        Mockito.when(httpClient.execute(Matchers.any(HttpUriRequest.class))).thenReturn(httpResponse);
        Mockito.when(httpResponse.getEntity()).thenReturn(httpEntity);
        String responseJson = "{ \"access_token\" : \"access_token_value_string\" }";
        InputStream stream = new ByteArrayInputStream(responseJson.getBytes());
        Mockito.when(httpEntity.getContent()).thenReturn(stream);
        String tokenEndPoint = "https://authserver.com/oauth/token";
        String clientId = "testClient";
        String jwt = builder.buildJwtString(clientId, "testClientSecret_testClientSecret_testClientSecret", tokenEndPoint);
        String tokenJson = builder.obtainToken(tokenEndPoint, clientId, jwt);
        Assert.assertEquals(responseJson, tokenJson);
    }
}
