## Licensing
```
Copyright (c) 2021 Reltio

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
```

### What is this repository for? ###

* This repository has sample java code to generate a JWT string by using HS256 signature algorithm and to obtain a access token by making a http call to the token endpoint of the authorization service.
* Please refer buildJwtString() method in jwt.JwtBuilder class for building JWT string.
* Also refer obtainToken() method in jwt.JwtBuilder to obtain the token from authorization service by client_secret_jwt authentication method.

* The sample program takes three arguments listed below
    *   Client ID - The client Id that should be used for authentication to obtain the access token
    *   Client Secret - The client secret that should be used for authentication to obtain the access token. This would be provided by the Reltio
    *   Token Endpoint - The token endpoint of authorization service 

* VERSION - 0.1.0
